# Scripts em `R` para estimar os indicadores de linha de base do ODS 11


## Tabela de Indicadores Nacionais

| #Indicador | Indicador | Base de dados | Ano | Calculado?
| ------ | ------ | ------ | ------ |------ |
|11-1-1|Proporcao da pop urbana em domicilios com onus do aluguel no orçamento familiar (ou outras componentes do déficit habitacional)|Pnad (IBGE)|2017|Ok
|11-1-2|Proporcao da pop urbana em domicilios precarios|Pnad (IBGE)|2017|Ok
|11-1-3|Percentual da população residente em aglomerados subnormais em relação a |Censo (IBGE)|2010|Ok
|11-2-1|Percentual de viagens feitas por meio de transporte público, a pé ou de bicicleta|PNS (IBGE)|2013|Ok
|11-2-2|Proporção do orçamento familiar comprometido com transporte público|POF (IBGE)|2008
|2|Percentual da população vivendo próxima (num raio de 1Km) a terminais e estações de transporte de média e alta capacidade (total e por faixa de renda)|censo|2010
|11-3-1| Percentual de municípios com Plano Diretor participativo|Munic (IBGE)|2015|Ok
|11-3-2|Percentual de municípios com conselhos municipais e Fóruns municipais setoriais|Munic (IBGE)|???|Ok
|11-3-3|Percentual de municípios que fazem orçamento participativo|Munic (IBGE)|2014|Ok
|11-4-1|Percentual de municípios com conselho municipal de cultura e patrimônio histórico|Munic (IBGE)|2012|Ok
|11-5-1|Percentual de população em áreas de risco|Censo (IBGE)/Cemaden|2010
|11-5-3|Percentual da população que recebe alerta de risco de desastres elaborado pelo MCTIC ou via SMS pela Defesa Civil|cemaden|16/17
|11-6-1|Percentual de municípios com Planos Municipais de Gestão Integrada de Resíduos Sólidos|Munic (IBGE)|???| Ok
|7|Percentual de pessoas residentes em domicílios cujo padrão urbanístico do entorno possui calçadas com rampas de acesso e áreas verdes nas faces de quadra dos domicílios|censo|2010
|A|Percentual de aglomerações urbanas com algum órgão de gestão metropolitana|reg. Adm.|16/17
|A|Percentual de Regiões Metropolitanas que instituíram Plano de Desenvolvimento Urbano Integrado|reg. Adm.|16/17
|11-B-1| Percentual de municípios com plano municipal de redução de riscos|Munic (IBGE)|???|Ok
|11-B-2|Percentual de municípios com ações e/ou instrumentos de gerenciamento de riscos|Munic (IBGE)|???|Ok



## Tabela de Indicadores Globais


| #Indicador | Indicador | Base de dados | Ano | Calculado?
| ------ | ------ | ------ | ------ |------ |
|11-1-1|Proporção de população urbana vivendo em assentamentos precários, assentamentos informais ou domicílios inadequados|[Censo (IBGE)](https://indicadoresods.ibge.gov.br/objetivo11/indicador1111)|2010|Ok
|11-5-1|Número de mortes, pessoas desaparecidas e pessoas diretamente afetadas atribuído a desastres por 100 mil habitantes|[IBGE + diversos](https://indicadoresods.ibge.gov.br/objetivo11/indicador1151)|2015-2017|Ok
|11-B-2|Proporção de governos locais que adotam e implementam estratégias locais de redução de risco de desastres em linha com as estratégias nacionais de redução de risco de desastres|[Munic (IBGE)](https://indicadoresods.ibge.gov.br/objetivo11/indicador11b2)|2013|Ok


